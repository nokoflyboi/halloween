package de.nicokst.cp;

import java.util.Random;

public class Tool {

    public static String random(int length) {
        String[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".split("");
        String f = "";
        Random random = new Random();
        for(int i = 0; i < length; i++) {
            f += chars[random.nextInt(chars.length)];
        }
        return f;
    }

}
