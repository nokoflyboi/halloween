package de.nicokst.cp;

import java.io.File;
import java.sql.*;

public class SQLite {

    private static SQLite instance;

    private Connection connection;

    private SQLite() {
        try {
            File file = new File("spooky.db");
            if(!file.exists()) {
                file.createNewFile();
            }
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:spooky.db");
            System.out.println("Local Database loaded successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update(String sql) {
        try {
            connection.createStatement().executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet query(String sql) {
        try {
            Statement st = connection.createStatement();
            return st.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static SQLite getInstance() {
        if(instance == null) {
            instance = new SQLite();
        }

        return instance;
    }



}
