package de.nicokst.cp;

import de.nicokst.fancy.Quote;

public class Constant {

    public static FileConfiguration CONFIG;

    public static MinecraftServer SERVER;

    public static String MASTER_PASSWORD = "2spooky4me";

    public static final Quote[] QUOTES = new Quote[] {
            new Quote("noko lemony", "it isn't spooky if it isn't dooty"),
            new Quote("Unknown", "When witches go riding, and black cats are seen, the moon laughs and whispers 'tis near Halloween."),
            new Quote("Arthur Conan Doyle", "Where there is no imagination there is no horror."),
            new Quote("Max, \"Hocus Pocus\"", "It's all just a bunch of hocus pocus!"),
            new Quote("Evan Peters", "I love Halloween, and I love that feeling: the cold air, the spooky dangers lurking around the corner."),
            new Quote("Tim Burton", "Every day is Halloween, isn't it? For some of us."),
            new Quote("Unknown", "During the day, I don't believe in ghosts. At night, I'm a little more open-minded."),
            new Quote("William Motherwell", "Men say that in this midnight hour, the disembodied have power.")
    };

}
