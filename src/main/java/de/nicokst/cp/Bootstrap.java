package de.nicokst.cp;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import de.nicokst.fancy.Quote;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import javax.security.auth.login.Configuration;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static spark.Spark.*;

public class Bootstrap {

    public static Random random;

    public static void main(String[] args) {
        System.out.println("Starting the spook ... ");
        random = new Random();

        Constant.SERVER = new MinecraftServer();

        Constant.CONFIG = new FileConfiguration("config.cfg");

        Constant.CONFIG.copyDefaults(new String[] {
                "#Changes don't apply during runtime",
                "#The hostname is only for displaying the full address in the console :-)",
                "launch-command=java -Xmx1G -Xms1G -Dcom.mojang.eula.agree=true -jar server.jar",
                "interface-port=8080",
                "master-password=2spooky4me",
                "hostname=localhost",
                "#happy spooks!"
        });

        Constant.MASTER_PASSWORD = Constant.CONFIG.getString("master-password");

        File mcservDir = new File("mcserv");
        if(!mcservDir.exists()) {
            mcservDir.mkdir();
        }

        port(Constant.CONFIG.getInt("interface-port"));

        SQLite.getInstance().update("CREATE TABLE IF NOT EXISTS `tokens` (\n" +
                "\t`id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
                "\t`token`\tTEXT NOT NULL\n" +
                ");");

        staticFileLocation("/public");

        before("/*", (req, res) -> {
            if(!req.pathInfo().equalsIgnoreCase("/mc/usage") && !req.pathInfo().equalsIgnoreCase("/mc/log")) {
                System.out.println("[" + req.ip() + "] " + req.requestMethod() + " -> " + req.pathInfo() + "(" + req.queryParams().toString() + ")");
            }
            res.header("Server", "JackOLantern/OpenSource");
            res.header("X-WELCOME", "Für das Halloween-Event");
            res.header("X-WELCOME-2", "Tweet an @nikokeist hihi");
        });

        post("/login-post", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("title", "Login to the real spook");

            String pswd = req.queryParams("password");
            if(pswd.equalsIgnoreCase(Constant.MASTER_PASSWORD)) {
                String token = Tool.random(32);

                res.cookie("/", "token", token, 3600 * 10, false, false);

                SQLite.getInstance().update("INSERT INTO `tokens` VALUES (NULL, '" + token + "');");
                res.redirect("/dashboard");
                return null;
            } else {
                model.put("loggedin", false);
                return new ModelAndView(model, "templates/login.vm");
            }

        }, new VelocityTemplateEngine());

        get("/dashboard", (req, res) -> {
            boolean ok = false;
            if(req.cookies().containsKey("token")) {
                String token = req.cookie("token");
                try {
                    ResultSet resultSet = SQLite.getInstance().query("SELECT * FROM `tokens` WHERE `token` = '" + token + "';");
                    if(resultSet.next()) {
                        ok = true;
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } else {
                redirect.get("/dashboard", "/login");
            }

            if(!ok) {
                res.redirect("/login");
            }

            Map<String, Object> model = new HashMap<>();
            model.put("title", "spooky snooky interface");
            Quote quote = Constant.QUOTES[random.nextInt(Constant.QUOTES.length)];
            model.put("quote", quote);
            model.put("server", Constant.SERVER);
            model.put("config", Constant.CONFIG);
            model.put("address", Constant.SERVER.getProperties().containsKey("server-port") ? Constant.CONFIG.getString("hostname") + ":" + Constant.SERVER.getProperties().get("server-port") : "Couldn't fetch data.");
            return new ModelAndView(model, "templates/index.vm");
        }, new VelocityTemplateEngine());

        get("/login", (req, res) -> {

            boolean ok = false;
            if(req.cookies().containsKey("token")) {
                String token = req.cookie("token");
                try {
                    ResultSet resultSet = SQLite.getInstance().query("SELECT * FROM `tokens` WHERE `token` = '" + token + "';");
                    if(resultSet.next()) {
                        ok = true;
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if(ok) {
                res.redirect("/dashboard");
            }

            Map<String, Object> model = new HashMap<>();
            model.put("title", "Login to the real spook");
            return new ModelAndView(model, "templates/login.vm");
        }, new VelocityTemplateEngine());

        get("/", (req, res) -> {
            res.redirect("/dashboard");
            return null;
        }, new VelocityTemplateEngine());

        before("/mc/*", (req, res) -> {
            boolean ok = false;
            if(req.cookies().containsKey("token")) {
                String token = req.cookie("token");
                try {
                    ResultSet resultSet = SQLite.getInstance().query("SELECT * FROM `tokens` WHERE `token` = '" + token + "';");
                    if(resultSet.next()) {
                        ok = true;
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if(!ok) {
                halt(401, "Unallowed");
            }
        });

        path("/mc", () -> {
            get("/start", (req, res) -> {
               if(!Constant.SERVER.isOnline()) {
                   Constant.SERVER.start();
                   return json("success", "Server was started successfully.");
               } else {
                   return json("warning", "Server is online.");
               }
            });
            get("/stop", (req, res) -> {
               if(Constant.SERVER.isOnline()) {
                   Constant.SERVER.stopServer();
                   return json("success", "Server was shut down successfully");
               } else {
                   return json("warning", "Server is offline.");
               }
            });
            get("/reboot", (req, res) -> {
               if(!Constant.SERVER.isOnline()) {
                   return json("warning", "Server isn't online.");
               } else {
                   Constant.SERVER.stopServer();
                   Constant.SERVER.startServer();
                   return json("success", "Server rebooted successfully");
               }
            });
            post("/cmd", (req, res) -> {
                if(!Constant.SERVER.isOnline()) {
                    return json("warning", "Server is offline.");
                } else {

                    Constant.SERVER.sendCommand(req.queryParams("command"));
                    return json("success", "Command sent!");
                }
            });
            get("/usage", (req, res) -> {
               if(Constant.SERVER.isOnline()) {
                   return "{\"cpu\":\"" + Constant.SERVER.getUsage()[0] + "\",\"memory\":\"" + Constant.SERVER.getUsage()[1] + "\"}";
               }
               return "--";
            });
            get("/log", (req, res) -> { return Constant.SERVER.getLog(); });
        });

        get("*", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("title", "boooh!");
            return new ModelAndView(model, "templates/not-found.vm");
        }, new VelocityTemplateEngine());

        for(int i = 0; i < 10; i++) {
            System.out.println();
        }
        System.out.println("spookz is up and spooking, have fun!");
        System.out.println("Webinterface is available at http://" + Constant.CONFIG.getString("hostname") + ":" + Constant.CONFIG.getInt("interface-port"));
        System.out.println("Log in with following Master Password: " + Constant.MASTER_PASSWORD);
    }


    private static String json(String a, String b) {
        return "{\"type\":\"" + a + "\",\"message\":\"" + b + "\"}";
    }

}
