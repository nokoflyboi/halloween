package de.nicokst.cp;

import java.io.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author noko lemony (https://project-leaf.net)
 *
 * Code ist teilweise aus Project-Leaf
 */
public class MinecraftServer extends Thread {

    private int pid;
    private Process process;

    private PrintWriter writer;

    public void startServer() {
        try {
            ProcessBuilder builder = new ProcessBuilder(Constant.CONFIG.getString("launch-command").split(" "));
            builder.directory(new File("mcserv/"));
            if(this.process == null) {
                this.process = builder.start();
                this.pid = getPid();
                System.out.println("[" + getName() + "] thread started (#" + this.pid + ")");
                this.writer = new PrintWriter(this.process.getOutputStream(), true);
                this.process.waitFor();
            } else {
                this.process.destroyForcibly();
                this.process = null;
                startServer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getLog() {
        String n = "";
        File logFile = new File("mcserv/logs/latest.log");
        if(logFile.exists()) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(logFile));
                String a = "";
                while((a = bufferedReader.readLine()) != null) {
                    n += a + "\r\n";
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            n = "'latest.log' not found, try starting the Server.";
        }
        return n;
    }

    public Map<String, String> getProperties() {
        Map<String, String> properties = new HashMap<>();
        File logFile = new File("mcserv/server.properties");
        if(logFile.exists()) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(logFile));
                String a = "";
                while((a = bufferedReader.readLine()) != null) {
                    if(!a.startsWith("#")) {
                        String key = a.split("=")[0];
                        String value = a.substring(key.length() + 1, a.length());
                        properties.put(key, value);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }

    public int getPid() {
        try {
            Field f = process.getClass().getDeclaredField("pid");
            if (!f.isAccessible()) {
                f.setAccessible(true);
            }
            this.pid = (int) f.get(this.process);
        } catch (Exception e) {
            this.pid = -1;
        }
        return pid;
    }

    /**
     * forces the process to destroy
     * "kills it"
     */
    public void stopServer() {
        if(this.process != null && this.process.isAlive()) {
            try {
                System.out.println("[" + getName() + "] thread destroyed (#" + getPid() + ")");
                this.process.destroyForcibly();
                interrupt();
            } catch (Exception e) {
            }
        }
    }

    public boolean isOnline() {
        return (this.process != null && this.process.isAlive());
    }

    public void sendCommand(String command) {
        this.writer.println(command);
    }
    public double[] getUsage() {
        double[] data = new double[2];
        ProcessBuilder builder = new ProcessBuilder("ps", "-p", "" + getPid(), "-o", "%cpu,%mem", "--no-headers");
        try {
            Process p = builder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String n = "";
            while ((n = reader.readLine()) != null) {
                data = format(n);
                double cpu = data[0];
                if(cpu > 100 && cpu < 200) {
                    cpu = cpu / 2;
                } else if (cpu > 200 && cpu < 300) {
                    cpu = cpu / 3;
                } else if (cpu > 300 && cpu < 400) {
                    cpu = cpu / 4;
                }
                data[0] = Math.abs(cpu);

            }
        } catch (IOException e) {
            data[0] = 0.0;
            data[1] = 0;
        }

        return data;
    }


    @Override
    public void run() {
        startServer();
    }

    private double[] format(String n) {
        String[] x = n.split(" ");
        double[] f = new double[2];
        int c = 0;
        for (int i = 0; i < x.length; i++) {
            if (!x[i].equalsIgnoreCase("")) {
                f[c] = Double.valueOf(x[i]);
                c++;
            }
        }
        return f;
    }

}
