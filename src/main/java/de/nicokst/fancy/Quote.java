package de.nicokst.fancy;

public class Quote {

    private String author, quote;

    public Quote(String author, String quote) {
        this.author = author;
        this.quote = quote;
    }

    public String getQuote() {
        return quote;
    }

    public String getAuthor() {
        return author;
    }
}
