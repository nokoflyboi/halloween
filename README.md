## Control Panel "spookz"

Das ist ein Control Panel erstellt für ein Halloween-Event von http://dev-talk.net

! Dieses Control Panel hat nur die nötigsten Sicherheitsvorkehrungen, ich (https://twitter.com/nikokeist) übernehme keinerlei weitere Verantwortung für die Sicherheit !

#### Tell meh

- geschrieben 100% in Java
- Web-Framework: Sparkjava
- Java 8
- Templating Engine: Velocity
- Bootstrap Theme: das was aussieht wie ubuntu
- Lokale Datenspeicherung über SQLite
- mit einem Easter-Egg :)

Das Panel kann nur die nötigsten Dinge, aber dafür ist es open Source ;)

- starten
- stoppen
- neustarten
- kommandos senden
- konsole anzeigen
- server.properties anzeigen
- auslastung anzeigen
- es zeigt paar schöne Zitate an ;)

#### Wie funktioniert der Spaß?

Ganz einfach, entweder Herunterladen und selbst compilen, oder die fertige .jar https://project-leaf.net/get/spookz.jar herunterladen.

Auf einem **Linux** Server hochladen, am besten in einen eigenen Ordner.

Dort anschließend ``java -jar <jarfile>.jar`` ausführen (in einem screen Empfohlen)

Optional kann man das ganze jetzt noch einmal schließen und die Konfiguration anpassen, **das Master Passwort sollte auf jeden Fall geändert werden**

Eure Minecraft-Server-Dateien könnt ihr nun in den Ordner "mcserv" platzieren


 -> FERTIG

Das Panel ist dann unter http://ip:8080/ erreichbar!